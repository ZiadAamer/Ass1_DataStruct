#ifndef FRACTION_H
#define FRACTION_H
#include <iostream>

using namespace std;

class Fraction
{
    private:
        int den,num;
    public:
        Fraction():den(0),num(0){}
        Fraction(int x,int y):den(y),num(x){}
        Fraction operator+(Fraction );
        Fraction operator*(Fraction );
        Fraction operator/(Fraction );
        Fraction operator-(Fraction );
       friend istream &operator >> (istream &, Fraction &);
       friend ostream &operator << (ostream &,const Fraction &);
        //comparing
        bool operator<(Fraction );
        bool operator>(Fraction );
        bool operator<=(Fraction );
        bool operator>=(Fraction );
        bool operator==(Fraction );

        void reduce();
        void view();

};

#endif // FRACTION_H
