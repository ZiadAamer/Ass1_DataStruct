#ifndef FRACTIONCALCULATOR_H
#define FRACTIONCALCULATOR_H
#include "Fraction.h"
#include <iostream>

using namespace std;

class FractionCalculator
{
    private:
        Fraction F;
        char op;
    public:
        FractionCalculator();
        FractionCalculator(Fraction,char,Fraction);
        Fraction getValue();
};

#endif // FRACTIONCALCULATOR_H
