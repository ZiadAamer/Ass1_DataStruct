#include "Fraction.h"


Fraction  Fraction::operator+(Fraction other)
{
    Fraction temp;
    temp.num=num*other.den+other.num*den;
    temp.den=den*other.den;
    return temp;
}
Fraction  Fraction::operator-(Fraction other)
{
    Fraction temp;
    temp.num=num*other.den-other.num*den;
    temp.den=den*other.den;
    return temp;
}
Fraction Fraction::operator*(Fraction other)
{
    Fraction temp;
    temp.num=num*other.num;
    temp.den=den*other.den;
    return temp;
}
Fraction Fraction::operator/(Fraction other)
{
    Fraction temp;
    temp.num=den*other.num;
    temp.den=num*other.den;
    return temp;
}
//comparing
bool Fraction::operator<(Fraction other)
{
    return ((double)num/den<(double)other.num/other.den);
}
bool Fraction::operator>(Fraction other)
{
    return ((double)num/den>(double)other.num/other.den);
}

bool Fraction::operator<=(Fraction other)
{
    return ((double)num/den<=(double)other.num/other.den);
}
bool Fraction::operator>=(Fraction other)
{
    return ((double)num/den>=(double)other.num/other.den);
}
bool Fraction::operator==(Fraction other)
{
    return ((double)num/den==(double)other.num/other.den);
}
istream &operator >> (istream &in, Fraction &f)
{
    char c;
    in>>f.num>>c>>f.den;
    return in;
}
ostream &operator << (ostream &out,const Fraction &f)
{
    out<<f.num<<"/"<<f.den<<endl;
    return out;
}
void Fraction::view()
{
    cout<<num<<"/"<<den<<endl;
}
int gcd(int x ,int y)
{
    if(x%y==0) return y;

    return gcd(y,x%y);
}
void Fraction::reduce()
{
    int x;
    if(num>den)
        x=gcd(num,den);
    else
        x=gcd(den,num);
        num/=x;
        den/=x;
}
