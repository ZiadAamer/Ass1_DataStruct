#include <iostream>
#include "Fraction.h"
#include "FractionCalculator.h"

using namespace std;

int main()
{
    Fraction f1,f2;
    char op;
    cin>>f1>>op>>f2;
    FractionCalculator f(f1,op,f2);
    f.getValue().view();
    return 0;
}
